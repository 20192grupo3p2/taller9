bin/terminal: obj/terminal.o
	gcc obj/terminal.o -o bin/terminal
obj/terminal.o: src/terminal.c
	gcc -g -Wall -c src/terminal.c -o obj/terminal.o

.PHONY: clean
clean:
	rm bin/terminal
	rm obj/*
