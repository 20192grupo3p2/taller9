#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char **argv) {
	pid_t pidHijo;
	pid_t statusHijo;
	pidHijo=fork();
	if(pidHijo== -1){
		printf("Error al crear proceso hijo");
		return -1;
	}
	else if(pidHijo==0) {
		char *args[] = {"bin/ls", NULL};
		execv("/bin/sh", args);
		printf("Se ha producido un error al ejecutar execv.\n");
	}
	waitpid(pidHijo, &statusHijo, 0);
	printf("Proceso del padre #%d y del hijo #%d terminados\n", getppid(), getpid());
	return 0;
}

